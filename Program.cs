//#region koneksi enkripsi
//using Microsoft.EntityFrameworkCore;
//using My_API.Additional;
//using My_API.Connection;
//using System.Security.Cryptography;
//using System.Text;

//var builder = WebApplication.CreateBuilder(args);

//#region Connection Database
//var ConfigMyKeyStringValue = builder.Configuration["MyKey"];
//var ConfigConnectionStringValue = builder.Configuration["ConnectionStrings:DefaultConnection"];
////var EncryptConnectionString = EncryptDecrypt.EncryptString(ConfigConnectionStringValue, ConfigMyKeyStringValue); //Buka jika diperlukan untuk Encrypt
//var DecryptConnectionString = EncryptDecrypt.DecryptString(ConfigConnectionStringValue, ConfigMyKeyStringValue);
//builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseSqlServer(DecryptConnectionString));
//builder.Services.AddDbContext<ApplicationDBContext>(options =>
//{
//	options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
//});
//#endregion

//// Add services to the container.

//builder.Services.AddControllers();
//// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();

//var app = builder.Build();

//// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//	app.UseSwagger();
//	app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

//app.Run();

//#endregion

#region koneksi tidak enkripsi
using Microsoft.EntityFrameworkCore;
using My_API.Connection;

public partial class Program
{
    static void Main()
    {
        var builder = WebApplication.CreateBuilder();
        #region Connection Database
        var ConfigConnectionStringValue = builder.Configuration["ConnectionStrings:DefaultConnection"];
        builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseSqlServer(ConfigConnectionStringValue));
        builder.Services.AddDbContext<ApplicationDBContext>(options =>
        {
            options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
        });
        #endregion

        builder.Services.AddControllers(); //Add services to the container.
        builder.Services.AddEndpointsApiExplorer(); //Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddSwaggerGen();
        var app = builder.Build();
        //Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();
        app.Run();
    }
}


#endregion