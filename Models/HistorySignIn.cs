﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace My_API.Models;

[Table("HistorySignIn")]
public partial class HistorySignIn
{
    [Key]
    public long Id { get; set; }

    public long Nik { get; set; }

    public DateTime LastLogged { get; set; }

    public virtual MasterEmployee? NikNavigation { get; set; } = null!;
}

//public partial class HistorySignIn
//{
//    public long Id { get; set; }
//    public long Nik { get; set; }
//    public DateTime LastLogged { get; set; }
//    public virtual MasterEmployee NikNavigation { get; set; } = null!;
//}
