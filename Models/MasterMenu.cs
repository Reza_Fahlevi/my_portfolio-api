﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace My_API.Models;

[Table("MasterMenu")]
public partial class MasterMenu
{
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    //static long AutoId = 0;
    public long? Id { get; set; } //= ++AutoId;

    //[Key]
    [StringLength(50)]
    //[Unicode(false)]
    public string? KodeMenu { get; set; } //= null!;

    [StringLength(50)]
    [Unicode(false)]
    public string? NamaMenu { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? JenisMenu { get; set; }

    [Column(TypeName = "decimal(18, 0)")]
    public decimal? HargaSatuan { get; set; }

    public virtual ICollection<DaftarPemesan> DaftarPemesans { get; set; } = new List<DaftarPemesan>();
}


//public partial class MasterMenu
//{
//    public long Id { get; set; }
//    public string KodeMenu { get; set; } = null!;
//    public string? NamaMenu { get; set; }
//    public string? JenisMenu { get; set; }
//    public decimal? HargaSatuan { get; set; }
//    public virtual ICollection<DaftarPemesan> DaftarPemesans { get; set; } = new List<DaftarPemesan>();
//}