﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace My_API.Models;

[Table("DaftarPemesan")]
public partial class DaftarPemesan
{
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    //static long AutoId = 0;
    public long? Id { get; set; } //= ++AutoId;

    //[Key]
    public string? KodePemesan { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NamaPemesan { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? KodeMenu { get; set; }

    public virtual MasterMenu? KodeMenuNavigation { get; set; } = null!;
}


//public partial class DaftarPemesan
//{
//    public long Id { get; set; }
//    public string KodePemesan { get; set; } = null!;
//    public string NamaPemesan { get; set; } = null!;
//    public string KodeMenu { get; set; } = null!;
//    public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
//}