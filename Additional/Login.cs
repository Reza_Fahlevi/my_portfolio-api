﻿using Microsoft.Build.Framework;

namespace My_API.Additional
{
	public partial class PayloadLogin
    {
		[Required]
		public string Username { get; set; }
		[Required]
		public string Password { get; set; }
		[Required]
		public string HakAkses { get; set; }
	}
}
