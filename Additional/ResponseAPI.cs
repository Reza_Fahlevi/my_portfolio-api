﻿using My_API.Models;
using Microsoft.Build.Framework;
//using System.ComponentModel.DataAnnotations;

namespace My_API.Additional
{
    public partial class ResponseAPI
    {
        #region Models : MasterMenu
        public class ResponseMasterMenu
        {
            public class ResponseGetAllMasterMenu
            {
                public string message { get; set; }
                public List<MasterMenu> data { get; set; }
            }

            public class ResponseAddSingleDataMasterMenu
            {
                public string message { get; set; }
                public MasterMenu data { get; set; }
            }

            public class ResponseAddMultipleDataMasterMenu
            {
                public string message { get; set; }
                public List<MasterMenu> data { get; set; }
            }

            public class ResponseUpdateSingleDataMasterMenu
            {
                public string message { get; set; }
                public MasterMenu data { get; set; }
            }

            public class ResponseUpdateMultipleDataMasterMenu
            {
                public string message { get; set; }
                public List<MasterMenu> data { get; set; }
            }

            public class ResponseDeleteSingleDataMasterMenu
            {
                public string message { get; set; }
                public MasterMenu data { get; set; }
            }

            public class ResponseDeleteMultipleDataMasterMenu
            {
                public string message { get; set; }
                public List<MasterMenu> data { get; set; }
            }

            public class ResponseGetSingleDataMasterMenu
            {
                public string message { get; set; }
                public MasterMenu data { get; set; }
            }

            public class ResponseGetMultipleDataMasterMenu
            {
                public string message { get; set; }
                public List<MasterMenu> data { get; set; }
            }

            public class MasterMenu
            {
                public long Id { get; set; }
                public string KodeMenu { get; set; } = null!;
                public string? NamaMenu { get; set; }
                public string? JenisMenu { get; set; }
                public decimal? HargaSatuan { get; set; }
                public virtual ICollection<DaftarPemesan> DaftarPemesans { get; set; } = new List<DaftarPemesan>();
				public virtual DaftarPemesan DaftarPemesanNavigation { get; set; } = null!;
				public virtual Pesanan PesananNavigation { get; set; } = null!;
				public virtual Transaksi TransaksiNavigation { get; set; } = null!;
			}

            public class PayloadAddDataMasterMenu
            {
                public string KodeMenu { get; set; } = null!;
                public string? NamaMenu { get; set; }
                public string? JenisMenu { get; set; }
                public decimal? HargaSatuan { get; set; }
                //public virtual ICollection<DaftarPemesan> DaftarPemesans { get; set; } = new List<DaftarPemesan>();
            }

            public class PayloadUpdateDataMasterMenu
            {
                public string KodeMenu { get; set; } = null!;
                public string? NamaMenu { get; set; }
                public string? JenisMenu { get; set; }
                public decimal? HargaSatuan { get; set; }
                //public virtual ICollection<DaftarPemesan> DaftarPemesans { get; set; } = new List<DaftarPemesan>();
            }
        }
        #endregion

        #region Models : MasterEmployee
        public class ResponseMasterEmployee
        {
            public class ResponseGetAllDataMasterEmployee
            {
                public string message { get; set; }
                public List<MasterEmployee> data { get; set; }
            }

            public class ResponseAddSingleDataMasterEmployee
            {
                public string message { get; set; }
                public MasterEmployee data { get; set; }
            }

            public class ResponseAddMultipleDataMasterEmployee
            {
                public string message { get; set; }
                public List<MasterEmployee> data { get; set; }
            }

            public class ResponseUpdateSingleDataMasterEmployee
            {
                public string message { get; set; }
                public MasterEmployee data { get; set; }
            }

            public class ResponseUpdateMultipleDataMasterEmployee
            {
                public string message { get; set; }
                public List<MasterEmployee> data { get; set; }
            }

            public class ResponseDeleteSingleDataMasterEmployee
            {
                public string message { get; set; }
                public MasterEmployee data { get; set; }
            }

            public class ResponseDeleteMultipleDataMasterEmployee
            {
                public string message { get; set; }
                public List<MasterEmployee> data { get; set; }
            }

            public class ResponseGetSingleDataMasterEmployee
            {
                public string message { get; set; }
                public MasterEmployee data { get; set; }
            }

            public class ResponseGetMultipleDataMasterEmployee
            {
                public string message { get; set; }
                public List<MasterEmployee> data { get; set; }
            }

            public class MasterEmployee
            {
                public long Id { get; set; }
                public long Nik { get; set; }
                public string? NamaLengkap { get; set; }
                public string? NamaPanggilan { get; set; }
                public string? TempatLahir { get; set; }
                public DateTime? TanggalLahir { get; set; }
                public string? JenisKelamin { get; set; }
                public string? AlamatLengkap { get; set; }
                public string? AlamatDomisili { get; set; }
                public string? NomerTelepon { get; set; }
                public string? Email1 { get; set; }
                public string? Email2 { get; set; }
                public string? Jabatan { get; set; }
                public string UsernameLogin { get; set; } = null!;
                public string PasswordLogin { get; set; } = null!;
                public string HakAkses { get; set; } = null!;
                public bool? IsActive { get; set; }
                public virtual ICollection<HistorySignIn> HistorySignIns { get; set; } = new List<HistorySignIn>();
                public virtual ICollection<Transaksi> Transaksis { get; set; } = new List<Transaksi>();
            }

            public class PayloadAddDataMasterEmployee
            {
                public long Nik { get; set; }
                public string? NamaLengkap { get; set; }
                public string? NamaPanggilan { get; set; }
                public string? TempatLahir { get; set; }
                public DateTime? TanggalLahir { get; set; }
                public string? JenisKelamin { get; set; }
                public string? AlamatLengkap { get; set; }
                public string? AlamatDomisili { get; set; }
                public string? NomerTelepon { get; set; }
                public string? Email1 { get; set; }
                public string? Email2 { get; set; }
                public string? Jabatan { get; set; }
                public string UsernameLogin { get; set; } = null!;
                public string PasswordLogin { get; set; } = null!;
                public string HakAkses { get; set; } = null!;
                public bool? IsActive { get; set; }
                //public virtual ICollection<HistorySignIn> HistorySignIns { get; set; } = new List<HistorySignIn>();
                //public virtual ICollection<Transaksi> Transaksis { get; set; } = new List<Transaksi>();
            }

            public class PayloadUpdateDataMasterEmployee
            {
                public long Nik { get; set; }
                public string? NamaLengkap { get; set; }
                public string? NamaPanggilan { get; set; }
                public string? TempatLahir { get; set; }
                public DateTime? TanggalLahir { get; set; }
                public string? JenisKelamin { get; set; }
                public string? AlamatLengkap { get; set; }
                public string? AlamatDomisili { get; set; }
                public string? NomerTelepon { get; set; }
                public string? Email1 { get; set; }
                public string? Email2 { get; set; }
                public string? Jabatan { get; set; }
                public string UsernameLogin { get; set; } = null!;
                public string PasswordLogin { get; set; } = null!;
                public string HakAkses { get; set; } = null!;
                public bool? IsActive { get; set; }
                //public virtual ICollection<HistorySignIn> HistorySignIns { get; set; } = new List<HistorySignIn>();
                //public virtual ICollection<Transaksi> Transaksis { get; set; } = new List<Transaksi>();
            }
        }
        #endregion

        #region Models : DaftarPemesan
        public class ResponseDaftarPemesan
        {
            public class ResponseGetAllDataDaftarPemesan
            {
                public string message { get; set; }
                public List<DaftarPemesan> data { get; set; }
            }

            public class ResponseAddSingleDataDaftarPemesan
            {
                public string message { get; set; }
                public DaftarPemesan data { get; set; }
            }

            public class ResponseAddMultipleDataDaftarPemesan
            {
                public string message { get; set; }
                public List<DaftarPemesan> data { get; set; }
            }

            public class ResponseUpdateSingleDataDaftarPemesan
            {
                public string message { get; set; }
                public DaftarPemesan data { get; set; }
            }

            public class ResponseUpdateMultipleDataDaftarPemesan
            {
                public string message { get; set; }
                public List<DaftarPemesan> data { get; set; }
            }

            public class ResponseDeleteSingleDataDaftarPemesan
            {
                public string message { get; set; }
                public DaftarPemesan data { get; set; }
            }

            public class ResponseDeleteMultipleDataDaftarPemesan
            {
                public string message { get; set; }
                public List<DaftarPemesan> data { get; set; }
            }

            public class ResponseGetSingleDataDaftarPemesan
            {
                public string message { get; set; }
                public DaftarPemesan data { get; set; }
            }

            public class ResponseGetMultipleDataDaftarPemesan
            {
                public string message { get; set; }
                public List<DaftarPemesan> data { get; set; }
            }

            public class DaftarPemesan
            {
                public long Id { get; set; }
                public string KodePemesan { get; set; } = null!;
                public string NamaPemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
				public virtual MasterMenu MasterMenuNavigation { get; set; } = null!;
				public virtual DaftarPemesan DaftarPemesanNavigation { get; set; } = null!;
				public virtual Pesanan PesananNavigation { get; set; } = null!;
				public virtual Transaksi TransaksiNavigation { get; set; } = null!;
			}

            public class PayloadAddDataDaftarPemesan
            {
                public string KodePemesan { get; set; } = null!;
                public string NamaPemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
                public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
            }

            public class PayloadUpdateDataDaftarPemesan
            {
                public string KodePemesan { get; set; } = null!;
                public string NamaPemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
                public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
            }
        }
        #endregion

        #region Models : HistorySignIn
        public class ResponseGetAllDataHistorySignIn
        {
            public string message { get; set; }
            public List<HistorySignIn> data { get; set; }
        }

        public class ResponseAddSingleDataHistorySignIn
        {
            public string message { get; set; }
            public HistorySignIn data { get; set; }
        }

        public class ResponseAddMultipleDataHistorySignIn
        {
            public string message { get; set; }
            public List<HistorySignIn> data { get; set; }
        }

        public class ResponseUpdateSingleDataHistorySignIn
        {
            public string message { get; set; }
            public HistorySignIn data { get; set; }
        }

        public class ResponseUpdateMultipleDataHistorySignIn
        {
            public string message { get; set; }
            public List<HistorySignIn> data { get; set; }
        }

        public class ResponseDeleteSingleDataHistorySignIn
        {
            public string message { get; set; }
            public HistorySignIn data { get; set; }
        }

        public class ResponseDeleteMultipleDataHistorySignIn
        {
            public string message { get; set; }
            public List<HistorySignIn> data { get; set; }
        }

        public class ResponseGetSingleDataHistorySignIn
        {
            public string message { get; set; }
            public HistorySignIn data { get; set; }
        }

        public class ResponseGetMultipleDataHistorySignIn
        {
            public string message { get; set; }
            public List<HistorySignIn> data { get; set; }
        }

        public class HistorySignIn
        {
            public long Id { get; set; }

            public long Nik { get; set; }

            public DateTime LastLogged { get; set; }

            public virtual MasterEmployee NikNavigation { get; set; } = null!;
        }

        public class PayloadAddHistorySignIn
        {
            public long Id { get; set; }
            public long Nik { get; set; }
            public DateTime LastLogged { get; set; }
            public virtual MasterEmployee NikNavigation { get; set; } = null!;
        }

        public class PayloadUpdateHistorySignIn
        {
            public long Id { get; set; }
            public long Nik { get; set; }
            public DateTime LastLogged { get; set; }
            public virtual MasterEmployee NikNavigation { get; set; } = null!;
        }
        #endregion

        #region Models : Pesanan
        public class ResponsePesanan
        {
			public class ResponseMenuPesanan
			{
				public string message { get; set; }
				public List<MenuPesanan> data { get; set; }
			}

			public class ResponseGetAllPesanan
            {
                public string message { get; set; }
                public List<Pesanan> data { get; set; }
            }

            public class ResponseAddSingleDataPesanan
            {
                public string message { get; set; }
                public Pesanan data { get; set; }
            }

            public class ResponseAddMultipleDataPesanan
            {
                public string message { get; set; }
                public List<Pesanan> data { get; set; }
            }

            public class ResponseUpdateSingleDataPesanan
            {
                public string message { get; set; }
                public Pesanan data { get; set; }
            }

            public class ResponseUpdateMultipleDataPesanan
            {
                public string message { get; set; }
                public List<Pesanan> data { get; set; }
            }

            public class ResponseDeleteSingleDataPesanan
            {
                public string message { get; set; }
                public Pesanan data { get; set; }
            }

            public class ResponseDeleteMultipleDataPesanan
            {
                public string message { get; set; }
                public List<Pesanan> data { get; set; }
            }

            public class ResponseGetSingleDataPesanan
            {
                public string message { get; set; }
                public Pesanan data { get; set; }
            }

            public class ResponseGetMultipleDataPesanan
            {
                public string message { get; set; }
                public List<Pesanan> data { get; set; }
            }

            public class Pesanan
            {
                public long Id { get; set; }
                public string KodeTransaksi { get; set; } = null!;
                public string KodePemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
                public long JumlahPesanan { get; set; }
                public decimal HargaSatuan { get; set; }
                public long Nik { get; set; }
                public virtual MasterMenu MasterMenuNavigation { get; set; } = null!;
                public virtual DaftarPemesan DaftarPemesanNavigation { get; set; } = null!;
                public virtual Transaksi TransaksiNavigation { get; set; } = null!;
                public virtual MasterEmployee MasterEmployeeNavigation { get; set; } = null!;
            }

            public class PayloadAddDataPesanan
            {
                public string KodeTransaksi { get; set; } = null!;
                public string KodePemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
                public long JumlahPesanan { get; set; }
                public decimal HargaSatuan { get; set; }
                public long Nik { get; set; }
                //public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
                //public virtual DaftarPemesan KodePemesanNavigation { get; set; } = null!;
                //public virtual Transaksi KodeTransaksiNavigation { get; set; } = null!;
                //public virtual MasterEmployee NikNavigation { get; set; } = null!;
            }

            public class PayloadUpdateDataPesanan
            {
                public string KodeTransaksi { get; set; } = null!;
                public string KodePemesan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
                public long JumlahPesanan { get; set; }
                public decimal HargaSatuan { get; set; }
                public long Nik { get; set; }
                //public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
                //public virtual DaftarPemesan KodePemesanNavigation { get; set; } = null!;
                //public virtual Transaksi KodeTransaksiNavigation { get; set; } = null!;
                //public virtual MasterEmployee NikNavigation { get; set; } = null!;
            }

			public class MenuPesanan
			{
				public long Id { get; set; }
				public string KodeTransaksi { get; set; } = null!;
				public string KodePemesan { get; set; } = null!;
				public string NamaPemesan { get; set; } = null!;
                public string JenisPesanan { get; set; } = null!;
                public string KodeMenu { get; set; } = null!;
				public string NamaMenu { get; set; } = null!;
				public long JumlahPesanan { get; set; }
				public string HargaSatuan { get; set; }
			}
		}
        #endregion

        #region Models : Transaksi
        public class ResponseTransaksi
        {
            public class ResponseGetAllTransaksi
            {
                public string message { get; set; }
                public List<Transaksi> data { get; set; }
            }

            public class ResponseAddSingleDataTransaksi
            {
                public string message { get; set; }
                public Transaksi data { get; set; }
            }

            public class ResponseAddMultipleDataTransaksi
            {
                public string message { get; set; }
                public List<Transaksi> data { get; set; }
            }

            public class ResponseUpdateSingleDataTransaksi
            {
                public string message { get; set; }
                public Transaksi data { get; set; }
            }

            public class ResponseUpdateMultipleDataTransaksi
            {
                public string message { get; set; }
                public List<Transaksi> data { get; set; }
            }

            public class ResponseDeleteSingleDataTransaksi
            {
                public string message { get; set; }
                public Transaksi data { get; set; }
            }

            public class ResponseDeleteMultipleDataTransaksi
            {
                public string message { get; set; }
                public List<Transaksi> data { get; set; }
            }

            public class ResponseGetSingleDataTransaksi
            {
                public string message { get; set; }
                public Transaksi data { get; set; }
            }

            public class ResponseGetMultipleDataTransaksi
            {
                public string message { get; set; }
                public List<Transaksi> data { get; set; }
            }

            public class Transaksi
            {
                public long Id { get; set; }

                public string KodeTransaksi { get; set; } = null!;

                public decimal TotalPembayaran { get; set; }

                public bool StatusPembayaran { get; set; }

                public DateTime TanggalPembayaran { get; set; }

                public long Nik { get; set; }

                public virtual MasterEmployee NikNavigation { get; set; } = null!;
            }

            public class PayloadAddDataTransaksi
            {
                public long Id { get; set; }

                public string KodeTransaksi { get; set; } = null!;

                public decimal TotalPembayaran { get; set; }

                public bool StatusPembayaran { get; set; }

                public DateTime TanggalPembayaran { get; set; }

                public long Nik { get; set; }

                //public virtual MasterEmployee NikNavigation { get; set; } = null!;
            }

            public class PayloadUpdateDataTransaksi
            {
                public long Id { get; set; }

                public string KodeTransaksi { get; set; } = null!;

                public decimal TotalPembayaran { get; set; }

                public bool StatusPembayaran { get; set; }

                public DateTime TanggalPembayaran { get; set; }

                public long Nik { get; set; }

                //public virtual MasterEmployee NikNavigation { get; set; } = null!;
            }
        }
        #endregion

        #region Response : Login
        public class ResponseLogin
        {
            [Required]
            public string Message { get; set; }
            [Required]
            public long GetStatus { get; set; }
            [Required]
            public string UserName { get; set; }
        }
        #endregion
	}
}
