﻿using System.Security.Cryptography;
using System.Text;

namespace My_API.Additional
{
    public class EncryptDecrypt
    {
        public static string EncryptString(string Data, string Passphrase)
        {
            try
            {
                byte[] InitializationVector = Encoding.ASCII.GetBytes(Passphrase);
                using (Aes AES = Aes.Create())
                {
                    AES.Key = Encoding.UTF8.GetBytes(Passphrase);
                    AES.IV = InitializationVector;
                    var SymmetricEncryptor = AES.CreateEncryptor(AES.Key, AES.IV);
                    using (var MemoryStream = new MemoryStream())
                    {
                        using (var CryptoStream = new CryptoStream(MemoryStream as Stream, SymmetricEncryptor, CryptoStreamMode.Write))
                        {
                            using (var StreamWriter = new StreamWriter(CryptoStream as Stream))
                            {
                                StreamWriter.Write(Data);
                            }
                            return Convert.ToBase64String(MemoryStream.ToArray());
                        }
                    }
                }
            }
            catch (NullReferenceException ErrorNullReference)
            {
                Console.WriteLine(ErrorNullReference.Message);
                throw;
            }
            catch (Exception ErrorException)
            {
                Console.WriteLine(ErrorException.Message);
                throw new ApplicationException(ErrorException.Message);
            }

            finally
            {
            }
        }

        public static string DecryptString(string CipherText, string Passphrase)
        {
            try
            {
                byte[] InitializationVector = Encoding.ASCII.GetBytes(Passphrase);
                byte[] Buffer = Convert.FromBase64String(CipherText);
                using (Aes AES = Aes.Create())
                {
                    AES.Key = Encoding.UTF8.GetBytes(Passphrase);
                    AES.IV = InitializationVector;
                    var Decryptor = AES.CreateDecryptor(AES.Key, AES.IV);
                    using (var MemoryStream = new MemoryStream(Buffer))
                    {
                        using (var CryptoStream = new CryptoStream(MemoryStream as Stream, Decryptor, CryptoStreamMode.Read))
                        {
                            using (var StreamReader = new StreamReader(CryptoStream as Stream))
                            {
                                return StreamReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (NullReferenceException ErrorNullReference)
            {
                Console.WriteLine(ErrorNullReference.Message);
                throw;
            }
            catch (Exception ErrorException)
            {
                Console.WriteLine(ErrorException.Message);
                throw new ApplicationException(ErrorException.Message);
            }
            finally
            {
            }
        }
    }
}
