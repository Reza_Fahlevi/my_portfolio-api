﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Connection;
using My_API.Models;
using static System.Net.Mime.MediaTypeNames;
using static System.Runtime.InteropServices.JavaScript.JSType;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace My_API.Controllers
{
    [Route("API/[controller]/[action]")]
	[ApiController]
	public class HistorySignInController : ControllerBase
	{
        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        //public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<HistorySignInController> Logger;

        public HistorySignInController(ILogger<HistorySignInController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        #region Property Result
        public static dynamic Result;
        public static List<dynamic>[] Null;
        #endregion

        /// <summary>
        /// HistorySignIn API CRUD : Create, Read, Update, Delete

        #region GetAllDataHistorySignIn : Successs
        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataHistorySignIn()
        {
            try
            {
                //var GetData = DBContext.HistorySignIns.OrderBy(x => x.Id).ToList();
                var GetData = DBContext.HistorySignIns.OrderBy(x => x.Id).AsQueryable();
                if (GetData.Count() > 0)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return Ok(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(HistorySignIn Payload)
        {
            try
            {
                var ObjectInsert = new HistorySignIn();

                if (DBContext.HistorySignIns.Count() < 0 || DBContext.HistorySignIns.Count() == 0)
                {
                    ObjectInsert.Id = 1;
                    ObjectInsert.Nik = Payload.Nik;
                    ObjectInsert.LastLogged = Payload.LastLogged;
                }
                else
                {
                    long GetLastId = DBContext.HistorySignIns.OrderByDescending(x => x.Id).First().Id;
                    long Id = GetLastId + 1;
                    ObjectInsert.Id = Id;
                    ObjectInsert.Nik = Payload.Nik;
                    ObjectInsert.LastLogged = Payload.LastLogged;
                }

                #region Call the database connection and then insert multiple data.
                DBContext.HistorySignIns.Add(ObjectInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<HistorySignIn> Payload)
        {
            try
            {
                long Id;
                long GetLastId;
                var ObjectInsert_TableEmpty = new HistorySignIn();
                var ObjectInsert_TableNotEmpty = new HistorySignIn();
                var ListDataMultiple = new List<HistorySignIn>();

                foreach (var x in Payload)
                {
                    if (DBContext.HistorySignIns.Count() < 0 || DBContext.HistorySignIns.Count() == 0)
                    {
                        ObjectInsert_TableEmpty.Id = 1;
                        ObjectInsert_TableEmpty.Nik = x.Nik;
                        ObjectInsert_TableEmpty.LastLogged = x.LastLogged;

                        #region Call the database connection and then insert multiple data.
                        DBContext.HistorySignIns.Add(ObjectInsert_TableEmpty);
                        DBContext.SaveChanges();
                        #endregion

                        ListDataMultiple.Add(ObjectInsert_TableEmpty);
                    }
                    else
                    {
                        GetLastId = DBContext.HistorySignIns.OrderByDescending(x => x.Id).First().Id;
                        Id = GetLastId + 1;

                        ObjectInsert_TableNotEmpty.Id = Id;
                        ObjectInsert_TableNotEmpty.Nik = x.Nik;
                        ObjectInsert_TableNotEmpty.LastLogged = x.LastLogged;

                        #region Call the database connection and then insert multiple data.
                        DBContext.HistorySignIns.Add(ObjectInsert_TableNotEmpty);
                        DBContext.SaveChanges();
                        #endregion

                        ListDataMultiple.Add(ObjectInsert_TableNotEmpty);
                    }
                }

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(HistorySignIn Payload)
        {
            #region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.HistorySignIns.Where(x => x.Nik == Payload.Nik).AsQueryable();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.Nik = Payload.Nik;
            //            x.NamaLengkap = Payload.NamaLengkap;
            //            x.NamaPanggilan = Payload.NamaPanggilan;
            //            x.TempatLahir = Payload.TempatLahir;
            //            x.TanggalLahir = Payload.TanggalLahir;
            //            x.JenisKelamin = Payload.JenisKelamin;
            //            x.AlamatLengkap = Payload.AlamatLengkap;
            //            x.AlamatDomisili = Payload.AlamatDomisili;
            //            x.NomerTelepon = Payload.NomerTelepon;
            //            x.Email1 = Payload.Email1;
            //            x.Email2 = Payload.Email2;
            //            x.Jabatan = Payload.Jabatan;
            //            x.UsernameLogin = Payload.UsernameLogin;
            //            x.PasswordLogin = Payload.PasswordLogin;
            //            x.HakAkses = Payload.HakAkses;
            //            x.IsActive = Payload.IsActive;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            #endregion

            #region Example 2 : With Single Data 
            try
            {
                var GetData = DBContext.HistorySignIns.Where(x => x.Nik == Payload.Nik).FirstOrDefault();

                if (GetData != null || GetData.Nik != null || GetData.Nik == Payload.Nik)
                {
                    GetData.LastLogged = Payload.LastLogged;
                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<HistorySignIn> Payload)
        {
            try
            {
                var ListDataUpdate = new List<HistorySignIn>();

                var GetData = DBContext.HistorySignIns.Where(p => Payload.Select(x => x.Id).Contains(p.Id)).Select(Hasil => new HistorySignIn
                {
                    Id = Hasil.Id,
                    Nik = Hasil.Nik,
                    LastLogged = Hasil.LastLogged,
                    //NikNavigation = Hasil.NikNavigation,
                }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.FirstOrDefault(x => x.Nik == Obj.Nik).Id;
                        var Nik = Payload.FirstOrDefault(x => x.Nik == Obj.Nik).Nik;
                        var LastLogged = Payload.FirstOrDefault(x => x.Nik == Obj.Nik).LastLogged;
                        //var NikNavigation = Payload.FirstOrDefault(x => x.Nik == Nik).NikNavigation;

                        var ObjectUpdate = new HistorySignIn 
                        {
                            Id = Id,
                            Nik = Nik,
                            LastLogged = LastLogged,
                            //NikNavigation = NikNavigation
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataByNIK : Success
        [HttpDelete("{NIK}"), Produces("application/json")]
        public IActionResult DeleteDataByNIK(long NIK)
        {
            try
            {
                var GetData = DBContext.HistorySignIns.Where(x => x.Nik == NIK).FirstOrDefault();
                if (GetData != null)
                {
                    DBContext.HistorySignIns.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByNIK : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByNIK(List<long> NIK)
        {
            try
            {
                var ObjectDelete = new HistorySignIn();
                var ListDataDelete = new List<HistorySignIn>();
                var StringJoin = string.Join(",", NIK);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetData = (from x in DBContext.HistorySignIns
                                   where x.Nik == (Convert.ToInt64(obj))
                                   select new HistorySignIn
                                   {
                                       Id = x.Id,
                                       Nik = x.Nik,
                                       LastLogged = x.LastLogged,
                                       NikNavigation = x.NikNavigation,
                                   }).AsQueryable();


                    if (GetData.Count() > 0)
                    {
                        foreach (var x in GetData)
                        {
                            ObjectDelete.Id = x.Id;
                            ObjectDelete.Nik = x.Nik;
                            ObjectDelete.LastLogged = x.LastLogged;
                            ObjectDelete.NikNavigation = x.NikNavigation;

                            ListDataDelete.Add(ObjectDelete);
                            DBContext.HistorySignIns.Entry(ObjectDelete).State = EntityState.Deleted;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = GetData
                        };
                        return NotFound(Result);
                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = ListDataDelete
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByNik : Success
        [HttpGet("{NIK}"), Produces("application/json")]
        public IActionResult GetDataByNik(long NIK)
        {
            try
            {
                var GetData = DBContext.HistorySignIns.FirstOrDefault(x => x.Nik == NIK);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<HistorySignIn>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.HistorySignIns.Where(x => x.Nik == Convert.ToInt64(Obj)).Single();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
		#endregion

		#region GetDataMultipleInt : Success
		[HttpGet, Produces("application/json")]
		public IActionResult GetDataMultipleInt(List<int> Payload)
		{
			try
			{
				var ListDataMultiple = new List<HistorySignIn>();
				var StringJoin = string.Join(",", Payload);
				var SplitData = StringJoin.Split(",");

				if (Payload.Count() > 0)
				{
					foreach (var Obj in SplitData)
					{
						var GetData = DBContext.HistorySignIns.Where(x => x.Id == Convert.ToInt64(Obj)).Single();
						if (GetData == null)
						{
							Result = new
							{
								Message = "Data not found !",
								Data = GetData
							};
							return NotFound(Result);
						}
						else
						{
							ListDataMultiple.Add(GetData);
						}
					}
					Result = new
					{
						Message = "Data successfully found !",
						Data = ListDataMultiple
					};
					return StatusCode(200, Result);
				}
				else
				{
					Result = new
					{
						Message = "Data not found !",
						Data = ListDataMultiple
					};
					return NotFound(Result);
				}
			}
			catch (Exception Error)
			{
				Result = new
				{
					StatusCode = 500,
					Message = Error.Message,
					Data = Null
				};
				Console.WriteLine(Error.Message);
				return StatusCode(500, Result);
			}
			finally
			{
			}
		}
		#endregion
	}
}
