﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Connection;
using My_API.Models;
using System.Security.Policy;
using static My_API.Additional.ResponseAPI.ResponsePesanan;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace My_API.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class TransaksiController : ControllerBase
    {
		#region Property Connection Database
		static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
		static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
		//public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
		public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

		private readonly ApplicationDBContext DBContext;
		private readonly ILogger<TransaksiController> Logger;

		public TransaksiController(ILogger<TransaksiController> logger, ApplicationDBContext dbcontext)
		{
			Logger = logger;
			DBContext = dbcontext;
		}
		#endregion

		#region Property Result
		public static dynamic Result;
		public static List<dynamic>[] Null;
        #endregion

        /// <summary>
        /// Transaksi API CRUD : Create, Read, Update, Delete

        #region GetAllDataTransaksi : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataTransaksi()
        {
            try
            {
                //var GetData = DBContext.Transaksis.OrderBy(x => x.Id).ToList();
                var GetData = DBContext.Transaksis.OrderBy(x => x.Id).AsQueryable();
                if (GetData.Count() > 0)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return Ok(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(Transaksi Payload)
        {
            try
            {
                var ObjectInsert = new Transaksi()
                {
                    KodeTransaksi = Payload.KodeTransaksi,
                    TotalPembayaran = Payload.TotalPembayaran,
                    StatusPembayaran = Payload.StatusPembayaran,
                    TanggalPembayaran = Payload.TanggalPembayaran,
                    Nik = Payload.Nik,
                };

                #region Call the database connection and then insert data.
                DBContext.Add(ObjectInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<Transaksi> Payload)
        {
            try
            {
                var ListDataInsert = new List<Transaksi>();
                var ListDataMultiple = new List<Transaksi>();

                foreach (var x in Payload)
                {
                    ListDataInsert.Add(new Transaksi
                    {
                        KodeTransaksi = x.KodeTransaksi,
                        TotalPembayaran = x.TotalPembayaran,
                        StatusPembayaran = x.StatusPembayaran,
                        TanggalPembayaran = x.TanggalPembayaran,
                        Nik = x.Nik,
                    });
                }
                ListDataMultiple.AddRange(ListDataInsert);

                #region Call the database connection and then insert multiple data.
                DBContext.Transaksis.AddRange((IEnumerable<Transaksi>)ListDataInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(Transaksi Payload)
        {
            #region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.Pesanans.Where(x => x.KodePemesan == Payload.KodePemesan).ToList();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.KodeTransaksi = Payload.KodeTransaksi;
            //            x.KodePemesan = Payload.KodePemesan;
            //            x.KodeMenu = Payload.KodeMenu;
            //            x.JumlahPesanan = Payload.JumlahPesanan;
            //            x.HargaSatuan = Payload.HargaSatuan;
            //            x.Nik = Payload.Nik;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            #endregion

            #region Example 2 : With Single Data    
            try
            {
                var DBContext = new ApplicationDBContext();
                var GetData = DBContext.Transaksis.Where(x => x.KodeTransaksi == Payload.KodeTransaksi).FirstOrDefault();
                if (GetData != null || GetData.KodeTransaksi != null || GetData.KodeTransaksi == Payload.KodeTransaksi || GetData.KodeTransaksi.Contains(Payload.KodeTransaksi))
                {
                    GetData.Id = Payload.Id;    
                    GetData.KodeTransaksi = Payload.KodeTransaksi;
                    GetData.TotalPembayaran = Payload.TotalPembayaran;
                    GetData.StatusPembayaran = Payload.StatusPembayaran;
                    GetData.TanggalPembayaran = Payload.TanggalPembayaran;
                    GetData.Nik = Payload.Nik;

                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<Transaksi> Payload)
        {
            try
            {
                var ListDataUpdate = new List<Transaksi>();

                var GetData = DBContext.Transaksis.Where(p => Payload.Select(x => x.KodeTransaksi).Contains(p.KodeTransaksi)).Select(Hasil => new Transaksi
                {
                   Id = Hasil.Id,
                   KodeTransaksi = Hasil.KodeTransaksi,
                   TotalPembayaran = Hasil.TotalPembayaran,
                   StatusPembayaran = Hasil.StatusPembayaran,
                   TanggalPembayaran = Hasil.TanggalPembayaran,
                   Nik = Hasil.Nik,
            }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().Id;
                        var KodeTransaksi = Payload?.Where(x => x?.KodeTransaksi == Obj?.KodeTransaksi).FirstOrDefault()?.KodeTransaksi;
                        var TotalPembayaran = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().TotalPembayaran;
                        var StatusPembayaran = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().StatusPembayaran;
                        var TanggalPembayaran = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().TanggalPembayaran;
                        var Nik = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().Nik;

                        var ObjectUpdate = new Transaksi
                        {
                            Id = Id,
                            KodeTransaksi = KodeTransaksi,
                            TotalPembayaran = TotalPembayaran,
                            StatusPembayaran = StatusPembayaran,
                            TanggalPembayaran = TanggalPembayaran,
                            Nik = Nik,
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataByKodeTransaksi : Success
        [HttpDelete("{KodeTransaksi}"), Produces("application/json")]
        public IActionResult DeleteDataByKodeTransaksi(string KodeTransaksi)
        {
            try
            {
                var GetData = DBContext.Transaksis.Include(x => x.NikNavigation).Where(x => x.KodeTransaksi == KodeTransaksi).FirstOrDefault();     
                if (GetData != null)
                {
                    DBContext.Transaksis.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByKodeTransaksi : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodeTransaksi(List<string> KodeTransaksi)
        {
            try
            {
                var ListDataDeletePesanan = new List<dynamic>();
                var ListDataDeleteTransaksi = new List<dynamic>();

                var StringJoin = string.Join(",", KodeTransaksi);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetDataPesanan = DBContext.Pesanans.Where(x => x.KodeTransaksi.Contains(obj)).FirstOrDefault();
                    var GetDataTransaksi = DBContext.Transaksis.Where(x => x.KodeTransaksi.Contains(obj)).FirstOrDefault();

                    if (GetDataPesanan != null && GetDataTransaksi != null)
                    {
                        DBContext.Pesanans.Entry(GetDataPesanan).State = EntityState.Deleted;
                        DBContext.SaveChanges();

                        DBContext.Transaksis.Entry(GetDataTransaksi).State = EntityState.Deleted;
                        DBContext.SaveChanges();

                        ListDataDeletePesanan.Add(GetDataPesanan);
                        ListDataDeleteTransaksi.Add(GetDataTransaksi);
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = new 
                            {
                                ListDataDeletePesanan,
                                ListDataDeleteTransaksi
                            }
                        };
                        return NotFound(Result);

                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = new
                    {
                        ListDataDeletePesanan,
                        ListDataDeleteTransaksi
                    }
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByKodeTransaksi : Success
        [HttpGet("{KodeTransaksi}"), Produces("application/json")]
        public IActionResult GetDataByKodeTransaksi(string KodeTransaksi)
        {
            try
            {
                var GetData = DBContext.Transaksis.FirstOrDefault(x => x.KodeTransaksi == KodeTransaksi);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Successs
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<Transaksi>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.Transaksis.Where(x => x.KodeTransaksi == Obj).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleInt : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            try
            {
                var ListDataMultiple = new List<Transaksi>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.Transaksis.Where(x => x.Id == Convert.ToInt64(Obj)).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetDataById For My_Portfolio : Success
        [HttpPost("{Id}"), Produces("application/json")]
        public IActionResult GetDataById(long Id)
        {
            try
            {
                var GetData = DBContext.Transaksis.FirstOrDefault(x => x.Id == Id);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion
    }
}
