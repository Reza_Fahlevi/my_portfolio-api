﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Connection;
using My_API.Models;

namespace My_API.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class DaftarPemesanController : ControllerBase
    {
		#region Property Connection Database
		static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
		static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
		//public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
		public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

		private readonly ApplicationDBContext DBContext;
		private readonly ILogger<DaftarPemesanController> Logger;

		public DaftarPemesanController(ILogger<DaftarPemesanController> logger, ApplicationDBContext dbcontext)
		{
			Logger = logger;
			DBContext = dbcontext;
		}
		#endregion

		#region Property Result
		public static dynamic Result;
		public static List<dynamic>[] Null;
		#endregion

		/// <summary>
		/// DaftarPemesan API CRUD : Create, Read, Update, Delete

		#region GetAllDaftarPemesan : Successs
		[HttpGet, Produces("application/json")]
		public IActionResult GetAllDataDaftarPemesan()
		{
			try
			{
				//var GetData = DBContext.HistorySignIns.OrderBy(x => x.Id).ToList();
				var GetData = DBContext.DaftarPemesans.OrderBy(x => x.Id).AsQueryable();
				if (GetData.Count() > 0)
				{
					Result = new
					{
						Message = "Data successfully found !",
						Data = GetData
					};
					return StatusCode(200, Result);
				}
				else
				{
					Result = new
					{
						Message = "Data not found !",
						Data = GetData
					};
					return Ok(Result);
				}
			}
			catch (Exception Error)
			{
				Result = new
				{
					StatusCode = 500,
					Message = Error.Message,
					Data = Null
				};
				Console.WriteLine(Error.Message);
				return StatusCode(500, Result);
			}
			finally
			{
			}
		}
        #endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(DaftarPemesan Payload)
        {
            try
            {
                var ObjectInsert = new DaftarPemesan()
                {
                    KodePemesan = Payload.KodePemesan,
                    NamaPemesan = Payload.NamaPemesan,
                    KodeMenu = Payload.KodeMenu,
                };

                #region Call the database connection and then insert data.
                DBContext.Add(ObjectInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success 
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<DaftarPemesan> Payload)
        {
            try
            {
                var ListDataInsert = new List<DaftarPemesan>();
                var ListDataMultiple = new List<DaftarPemesan>();

                foreach (var x in Payload)
                {
                    ListDataInsert.Add(new DaftarPemesan
                    {
                        KodePemesan = x.KodePemesan,
                        NamaPemesan = x.NamaPemesan,
                        KodeMenu    = x.KodeMenu,
                    });
                }
                ListDataMultiple.AddRange(ListDataInsert);

                #region Call the database connection and then insert multiple data.
                DBContext.DaftarPemesans.AddRange(ListDataInsert);
                DBContext.SaveChanges();
                //DBContext.Transaksis.AddRange((IEnumerable<DaftarPemesan>)ListDataInsert);
                //DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(DaftarPemesan Payload)
        {
            #region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.Pesanans.Where(x => x.KodePemesan == Payload.KodePemesan).ToList();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.KodeTransaksi = Payload.KodeTransaksi;
            //            x.KodePemesan = Payload.KodePemesan;
            //            x.KodeMenu = Payload.KodeMenu;
            //            x.JumlahPesanan = Payload.JumlahPesanan;
            //            x.HargaSatuan = Payload.HargaSatuan;
            //            x.Nik = Payload.Nik;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            #endregion

            #region Example 2 : With Single Data    
            try
            {
                var DBContext = new ApplicationDBContext();
                var GetData = DBContext.DaftarPemesans.Where(x => x.KodePemesan == Payload.KodePemesan).FirstOrDefault();
                if (GetData != null || GetData.KodePemesan != null || GetData.KodePemesan == Payload.KodePemesan || GetData.KodePemesan.Contains(Payload.KodePemesan))
                {
                    //GetData.Id = Payload.Id;
                    GetData.KodePemesan = Payload.KodePemesan;
                    GetData.NamaPemesan = Payload.NamaPemesan;
                    GetData.KodeMenu = Payload.KodeMenu;
                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<DaftarPemesan> Payload)
        {
            try
            {
                var ListDataUpdate = new List<DaftarPemesan>();

                var GetData = DBContext.DaftarPemesans.Where(p => Payload.Select(x => x.KodePemesan).Contains(p.KodePemesan)).Select(Hasil => new DaftarPemesan
                {
                    Id = Hasil.Id,
                    KodePemesan = Hasil.KodePemesan,
                    NamaPemesan = Hasil.NamaPemesan,
                    KodeMenu = Hasil.KodeMenu,
                }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.Where(x => x.KodePemesan == Obj.KodePemesan).FirstOrDefault().Id;
                        var KodePemesan = Payload?.Where(x => x?.KodePemesan == Obj?.KodePemesan).FirstOrDefault()?.KodePemesan;
                        var NamaPemesan = Payload.Where(x => x.KodePemesan == Obj.KodePemesan).FirstOrDefault().NamaPemesan;
                        var KodeMenu = Payload.Where(x => x.KodePemesan == Obj.KodePemesan).FirstOrDefault().KodeMenu;

                        var ObjectUpdate = new DaftarPemesan
                        {
                            Id = Id,
                            KodePemesan = KodePemesan,
                            NamaPemesan = NamaPemesan,
                            KodeMenu = KodeMenu,
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataByKodePemesan : Success
        [HttpDelete("{KodePemesan}"), Produces("application/json")]
        public IActionResult DeleteDataByKodePemesan(string KodePemesan)
        {
            try
            {
                var GetData = DBContext.DaftarPemesans.Where(x => x.KodePemesan == KodePemesan).FirstOrDefault();
                //var GetData = DBContext.DaftarPemesans.Include(x => x.NikNavigation).Where(x => x.KodePemesan == KodePemesan).FirstOrDefault();
                if (GetData != null)
                {
                    DBContext.DaftarPemesans.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByKodePemesan : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodePemesan(List<string> KodePemesan)
        {
            try
            {
                var ObjectDelete = new DaftarPemesan();
                var ListDataDelete = new List<DaftarPemesan>();
                var StringJoin = string.Join(",", KodePemesan);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetData = (from x in DBContext.DaftarPemesans.Where(x => x.KodePemesan.Contains(obj))
                                   select new DaftarPemesan
                                   {
                                       Id = x.Id,
                                       KodePemesan = x.KodePemesan,
                                       NamaPemesan = x.NamaPemesan,
                                       KodeMenu = x.KodeMenu,
                                   }).AsQueryable();

                    if (GetData.Count() > 0)
                    {
                        foreach (var x in GetData)
                        {
                            ObjectDelete.Id = x.Id;
                            ObjectDelete.KodePemesan = x.KodePemesan;
                            ObjectDelete.NamaPemesan = x.NamaPemesan;
                            ObjectDelete.KodeMenu = x.KodeMenu;

                            ListDataDelete.Add(ObjectDelete);
                            DBContext.DaftarPemesans.Entry(ObjectDelete).State = EntityState.Deleted;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = GetData
                        };
                        return NotFound(Result);
                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = ListDataDelete
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByKodePemesan : Success
        [HttpGet("{KodePemesan}"), Produces("application/json")]
        public IActionResult GetDataByKodePemesan(string KodePemesan)
        {
            try
            {
                var GetData = DBContext.DaftarPemesans.FirstOrDefault(x => x.KodePemesan == KodePemesan);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<DaftarPemesan>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.DaftarPemesans.Where(x => x.KodePemesan == Obj).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleInt : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            try
            {
                var ListDataMultiple = new List<DaftarPemesan>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.DaftarPemesans.Where(x => x.Id == Convert.ToInt64(Obj)).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetDataById For My_Portfolio : Success
        [HttpPost("{Id}"), Produces("application/json")]
        public IActionResult GetDataById(long Id)
        {
            try
            {
                var GetData = DBContext.DaftarPemesans.FirstOrDefault(x => x.Id == Id);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion


    }
}
