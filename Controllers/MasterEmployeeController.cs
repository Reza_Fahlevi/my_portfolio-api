﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Additional;
using My_API.Connection;
using My_API.Interface;
using My_API.Models;
using My_API.Services;

namespace My_API.Controllers
{
	[Route("API/[controller]/[action]")]
	[ApiController]
	public class MasterEmployeeController : ControllerBase
	{
        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        //public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<MasterEmployeeController> Logger;

        public MasterEmployeeController(ILogger<MasterEmployeeController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        #region Property Result
        public static dynamic Result;
        public static List<dynamic>[] Null;
        #endregion

        /// <summary>
        /// MasterEmployee API CRUD : Create, Read, Update, Delete

        #region GetAllDataMasterEmployee : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterEmployee()
        {
            try
            {
                //var GetData = DBContext.MasterEmployees.OrderBy(x => x.Id).ToList();
               var  GetData = DBContext.MasterEmployees.OrderBy(x => x.Id).AsQueryable();
                if (GetData.Count() > 0)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return Ok(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(MasterEmployee Payload)
        {
            try
            {
                var ObjectInsert = new MasterEmployee()
                {
                    Nik = Payload.Nik,
                    NamaLengkap = Payload.NamaLengkap,
                    NamaPanggilan = Payload.NamaPanggilan,
                    TempatLahir = Payload.TempatLahir,
                    TanggalLahir = Payload.TanggalLahir,
                    JenisKelamin = Payload.JenisKelamin,
                    AlamatLengkap = Payload.AlamatLengkap,
                    AlamatDomisili = Payload.AlamatDomisili,
                    NomerTelepon = Payload.NomerTelepon,
                    Email1 = Payload.Email1,
                    Email2 = Payload.Email2,
                    Jabatan = Payload.Jabatan,
                    UsernameLogin = Payload.UsernameLogin,
                    PasswordLogin = Payload.PasswordLogin,
                    HakAkses = Payload.HakAkses,
                    IsActive = Payload.IsActive
                };

                #region Call the database connection and then insert data.
                DBContext.Add(ObjectInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<MasterEmployee> Payload)
        {
            try
            {
                var ListDataInsert = new List<MasterEmployee>();
                var ListDataMultiple = new List<MasterEmployee>();

                foreach (var x in Payload)
                {
                    ListDataInsert.Add(new MasterEmployee
                    {
                        Nik = x.Nik,
                        NamaLengkap = x.NamaLengkap,
                        NamaPanggilan = x.NamaPanggilan,
                        TempatLahir = x.TempatLahir,
                        TanggalLahir = x.TanggalLahir,
                        JenisKelamin = x.JenisKelamin,
                        AlamatLengkap = x.AlamatLengkap,
                        AlamatDomisili = x.AlamatDomisili,
                        NomerTelepon = x.NomerTelepon,
                        Email1 = x.Email1,
                        Email2 = x.Email2,
                        Jabatan = x.Jabatan,
                        UsernameLogin = x.UsernameLogin,
                        PasswordLogin = x.PasswordLogin,
                        HakAkses = x.HakAkses,
                        IsActive = x.IsActive
                    });
                }

                ListDataMultiple.AddRange(ListDataInsert);

                #region Call the database connection and then insert multiple data.
                DBContext.MasterEmployees.AddRange(ListDataInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(MasterEmployee Payload)
        {
            #region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Payload.Nik).AsQueryable();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.Nik = Payload.Nik;
            //            x.NamaLengkap = Payload.NamaLengkap;
            //            x.NamaPanggilan = Payload.NamaPanggilan;
            //            x.TempatLahir = Payload.TempatLahir;
            //            x.TanggalLahir = Payload.TanggalLahir;
            //            x.JenisKelamin = Payload.JenisKelamin;
            //            x.AlamatLengkap = Payload.AlamatLengkap;
            //            x.AlamatDomisili = Payload.AlamatDomisili;
            //            x.NomerTelepon = Payload.NomerTelepon;
            //            x.Email1 = Payload.Email1;
            //            x.Email2 = Payload.Email2;
            //            x.Jabatan = Payload.Jabatan;
            //            x.UsernameLogin = Payload.UsernameLogin;
            //            x.PasswordLogin = Payload.PasswordLogin;
            //            x.HakAkses = Payload.HakAkses;
            //            x.IsActive = Payload.IsActive;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            #endregion

            #region Example 2 : With Single Data 
            try
            {
                var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Payload.Nik).FirstOrDefault();

                if (GetData != null || GetData.Nik != null || GetData.Nik == Payload.Nik)
                {
                    GetData.Nik = Payload.Nik;
                    GetData.NamaLengkap = Payload.NamaLengkap;
                    GetData.NamaPanggilan = Payload.NamaPanggilan;
                    GetData.TempatLahir = Payload.TempatLahir;
                    GetData.TanggalLahir = Payload.TanggalLahir;
                    GetData.JenisKelamin = Payload.JenisKelamin;
                    GetData.AlamatLengkap = Payload.AlamatLengkap;
                    GetData.AlamatDomisili = Payload.AlamatDomisili;
                    GetData.NomerTelepon = Payload.NomerTelepon;
                    GetData.Email1 = Payload.Email1;
                    GetData.Email2 = Payload.Email2;
                    GetData.Jabatan = Payload.Jabatan;
                    GetData.UsernameLogin = Payload.UsernameLogin;
                    GetData.PasswordLogin = Payload.PasswordLogin;
                    GetData.HakAkses = Payload.HakAkses;
                    GetData.IsActive = Payload.IsActive;

                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<MasterEmployee> Payload)
        {
            try
            {
                var ListDataUpdate = new List<MasterEmployee>();

                var GetData = DBContext.MasterEmployees.Where(p => Payload.Select(x => x.Nik).Contains(p.Nik)).Select(Hasil => new MasterEmployee
                {
                    Id = Hasil.Id,
                    Nik = Hasil.Nik,
                    NamaLengkap = Hasil.NamaLengkap,
                    NamaPanggilan = Hasil.NamaPanggilan,
                    TempatLahir = Hasil.TempatLahir,
                    TanggalLahir = Hasil.TanggalLahir,
                    JenisKelamin = Hasil.JenisKelamin,
                    AlamatLengkap = Hasil.AlamatLengkap,
                    AlamatDomisili = Hasil.AlamatDomisili,
                    NomerTelepon = Hasil.NomerTelepon,
                    Email1 = Hasil.Email1,
                    Email2 = Hasil.Email2,
                    Jabatan = Hasil.Jabatan,
                    UsernameLogin = Hasil.UsernameLogin,
                    PasswordLogin = Hasil.PasswordLogin,
                    HakAkses = Hasil.HakAkses,
                    IsActive = Hasil.IsActive,
                }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.Where(x => x.Nik == Obj.Nik).FirstOrDefault().Id;
                        var NIK = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Nik;
                        var NamaLengkap = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NamaLengkap;
                        var NamaPanggilan = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NamaPanggilan;
                        var TempatLahir = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.TempatLahir;
                        var TanggalLahir = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.TanggalLahir;
                        var JenisKelamin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.JenisKelamin;
                        var AlamatLengkap = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.AlamatLengkap;
                        var AlamatDomisili = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.AlamatDomisili;
                        var NomerTelepon = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NomerTelepon;
                        var Email1 = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Email1;
                        var Email2 = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Email2;
                        var Jabatan = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.Jabatan;
                        var UsernameLogin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.UsernameLogin;
                        var PasswordLogin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.PasswordLogin;
                        var HakAkses = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.HakAkses;
                        var IsActive = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.IsActive;

                        var ObjectUpdate = new MasterEmployee
                        {
                            Id = Id,
                            Nik = NIK,
                            NamaLengkap = NamaLengkap,
                            NamaPanggilan = NamaPanggilan,
                            TempatLahir = TempatLahir,
                            TanggalLahir = TanggalLahir,
                            JenisKelamin = JenisKelamin,
                            AlamatLengkap = AlamatLengkap,
                            AlamatDomisili = AlamatDomisili,
                            NomerTelepon = NomerTelepon,
                            Email1 = Email1,
                            Email2 = Email2,
                            Jabatan = Jabatan,
                            UsernameLogin = UsernameLogin,
                            PasswordLogin = PasswordLogin,
                            HakAkses = HakAkses,
                            IsActive = IsActive,
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataByNIK : Success
        [HttpDelete("{NIK}"), Produces("application/json")]
        public IActionResult DeleteDataByNIK(long NIK)
        {
            try
            {
                var GetData = DBContext.MasterEmployees.Where(x => x.Nik == NIK).FirstOrDefault();
                if (GetData != null)
                {
                    DBContext.MasterEmployees.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByNIK : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByNIK(List<long> NIK)
        {
            try
            {
                var ObjectDelete = new MasterEmployee();
                var ListDataDelete = new List<MasterEmployee>();
                var StringJoin = string.Join(",", NIK);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetData = (from x in DBContext.MasterEmployees
                                   where x.Nik.HasValue && x.Nik.Value.ToString().Contains(obj)
                                   select new MasterEmployee
                                   {
                                       Id = x.Id,
                                       Nik = x.Nik,
                                       NamaLengkap = x.NamaLengkap,
                                       NamaPanggilan = x.NamaPanggilan,
                                       TempatLahir = x.TempatLahir,
                                       TanggalLahir = x.TanggalLahir,
                                       JenisKelamin = x.JenisKelamin,
                                       AlamatLengkap = x.AlamatLengkap,
                                       AlamatDomisili = x.AlamatDomisili,
                                       NomerTelepon = x.NomerTelepon,
                                       Email1 = x.Email1,
                                       Email2 = x.Email2,
                                       Jabatan = x.Jabatan,
                                       UsernameLogin = x.UsernameLogin,
                                       PasswordLogin = x.PasswordLogin,
                                       HakAkses = x.HakAkses,
                                       IsActive = x.IsActive,
                                   }).AsQueryable();


                    if (GetData.Count() > 0)
                    {
                        foreach (var x in GetData)
                        {
                            ObjectDelete.Id = x.Id;
                            ObjectDelete.Nik = x.Nik;
                            ObjectDelete.NamaLengkap = x.NamaLengkap;
                            ObjectDelete.NamaPanggilan = x.NamaPanggilan;
                            ObjectDelete.TempatLahir = x.TempatLahir;
                            ObjectDelete.TanggalLahir = x.TanggalLahir;
                            ObjectDelete.JenisKelamin = x.JenisKelamin;
                            ObjectDelete.AlamatLengkap = x.AlamatLengkap;
                            ObjectDelete.AlamatDomisili = x.AlamatDomisili;
                            ObjectDelete.NomerTelepon = x.NomerTelepon;
                            ObjectDelete.Email1 = x.Email1;
                            ObjectDelete.Email2 = x.Email2;
                            ObjectDelete.Jabatan = x.Jabatan;
                            ObjectDelete.UsernameLogin = x.UsernameLogin;
                            ObjectDelete.PasswordLogin = x.PasswordLogin;
                            ObjectDelete.HakAkses = x.HakAkses;
                            ObjectDelete.IsActive = x.IsActive;

                            ListDataDelete.Add(ObjectDelete);
                            DBContext.MasterEmployees.Entry(ObjectDelete).State = EntityState.Deleted;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = GetData
                        };
                        return NotFound(Result);
                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = ListDataDelete
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByNik : Success
        [HttpGet("{NIK}"), Produces("application/json")]
        public IActionResult GetDataByNik(long NIK)
        {
            try
            {
                var GetData = DBContext.MasterEmployees.FirstOrDefault(x => x.Nik == NIK);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByUsernameLogin : Success
        [HttpGet("{UsernameLogin}"), Produces("application/json")]
        public IActionResult GetDataByUsernameLogin(string UsernameLogin)
        {
            try
            {
                var GetData = DBContext.MasterEmployees.FirstOrDefault(x => x.UsernameLogin == UsernameLogin || x.UsernameLogin.Contains(UsernameLogin));
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Success 
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<MasterEmployee>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Convert.ToInt64(Obj)).Single();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleInt : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            try
            {
                var ListDataMultiple = new List<MasterEmployee>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.MasterEmployees.Where(x => x.Id == Convert.ToInt64(Obj)).Single();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetDataById For My_Portfolio : Success
        [HttpPost("{Id}"), Produces("application/json")]
        public IActionResult GetDataById(long Id)
        {
            try
            {
                var GetData = DBContext.MasterEmployees.FirstOrDefault(x => x.Id == Id);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion
    }
}
