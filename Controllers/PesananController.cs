﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Connection;
using My_API.Models;

namespace My_API.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class PesananController : ControllerBase
    {
		#region Property Connection Database
		static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
		static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
		//public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
		public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
		public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

		private readonly ApplicationDBContext DBContext;
		private readonly ILogger<PesananController> Logger;

		public PesananController(ILogger<PesananController> logger, ApplicationDBContext dbcontext)
		{
			Logger = logger;
			DBContext = dbcontext;
		}
		#endregion

		#region Property Result
		public static dynamic Result;
		public static List<dynamic>[] Null;
        #endregion

        /// <summary>
        /// Pesanan API CRUD : Create, Read, Update, Delete

        #region GetAllDataPesanan : Success
        [HttpGet, Produces("application/json")]
		public IActionResult GetAllDataPesanan()
		{
			try
			{
				//var GetData = DBContext.HistorySignIns.OrderBy(x => x.Id).ToList();
				var GetData = DBContext.Pesanans.OrderBy(x => x.Id).AsQueryable();
				if (GetData.Count() > 0)
				{
					Result = new
					{
						Message = "Data successfully found !",
						Data = GetData
					};
					return StatusCode(200, Result);
				}
				else
				{
					Result = new
					{
						Message = "Data not found !",
						Data = GetData
					};
					return Ok(Result);
				}
			}
			catch (Exception Error)
			{
				Result = new
				{
					StatusCode = 500,
					Message = Error.Message,
					Data = Null
				};
				Console.WriteLine(Error.Message);
				return StatusCode(500, Result);
			}
			finally
			{
			}
		}
		#endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(Pesanan Payload)
        {
            try
            {

                dynamic ObjectInsert;
                if (DBContext.Pesanans.Count() < 0 || DBContext.Pesanans.Count() == 0)
                {
                    ObjectInsert = new Pesanan
                    {
                        Id = 1,
                        KodeTransaksi = Payload.KodeTransaksi,
                        KodePemesan = Payload.KodePemesan,
                        KodeMenu = Payload.KodeMenu,
                        JumlahPesanan = Payload.JumlahPesanan,
                        HargaSatuan = Payload.HargaSatuan,
                        Nik = Payload.Nik,
                    };

                    #region Call the database connection and then insert data.
                    DBContext.Add(ObjectInsert);
                    DBContext.SaveChanges();
                    #endregion
                }
                else
                {

                    var GetLastId = DBContext.Pesanans.OrderByDescending(x => x.Id).First().Id;
                    var Id = GetLastId + 1;

                    ObjectInsert = new Pesanan
                    {
                        Id = Id,
                        KodeTransaksi = Payload.KodeTransaksi,
                        KodePemesan = Payload.KodePemesan,
                        KodeMenu = Payload.KodeMenu,
                        JumlahPesanan = Payload.JumlahPesanan,
                        HargaSatuan = Payload.HargaSatuan,
                        Nik = Payload.Nik,
                    };

                    #region Call the database connection and then insert data.
                    DBContext.Add(ObjectInsert);
                    DBContext.SaveChanges();
                    #endregion
                }



                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<Pesanan> Payload)
        {
            try
            {
                var ListDataMultiple = new List<Pesanan>();

                if (DBContext.Pesanans.Count() > 0 || DBContext.Pesanans.Count() != 0)
                {
                    foreach (var x in Payload)
                    {
                        var GetLastId = DBContext.Pesanans.OrderByDescending(x => x.Id).First().Id;
                        var Id = GetLastId + 1;

                        var ObjectInsert = new Pesanan 
                        {
                            Id = Id,
                            KodeTransaksi = x.KodeTransaksi,
                            KodePemesan = x.KodePemesan,
                            KodeMenu = x.KodeMenu,
                            JumlahPesanan = x.JumlahPesanan,
                            HargaSatuan = x.HargaSatuan,
                            Nik = x.Nik,
                        };

                        #region Call the database connection and then insert multiple data.
                        DBContext.Pesanans.Add(ObjectInsert);
                        DBContext.SaveChanges();
                        #endregion

                        ListDataMultiple.Add(ObjectInsert);
                    }
                }

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success 
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(Pesanan Payload)
        {
            //#region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.Pesanans.Where(x => x.KodePemesan == Payload.KodePemesan).ToList();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.KodeTransaksi = Payload.KodeTransaksi;
            //            x.KodePemesan = Payload.KodePemesan;
            //            x.KodeMenu = Payload.KodeMenu;
            //            x.JumlahPesanan = Payload.JumlahPesanan;
            //            x.HargaSatuan = Payload.HargaSatuan;
            //            x.Nik = Payload.Nik;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            //#endregion

            #region Example 2 : With Single Data    
            try
            {
                var DBContext = new ApplicationDBContext();
                var GetData = DBContext.Pesanans.Where(x => x.KodeTransaksi == Payload.KodeTransaksi).FirstOrDefault();
                if (GetData != null || GetData.KodeTransaksi != null || GetData.KodeTransaksi == Payload.KodeTransaksi || GetData.KodeTransaksi.Contains(Payload.KodeTransaksi))
                {
                    GetData.Id = Payload.Id;
                    GetData.KodeTransaksi = Payload.KodeTransaksi;
                    GetData.KodePemesan = Payload.KodePemesan;
                    GetData.KodeMenu = Payload.KodeMenu;
                    GetData.JumlahPesanan = Payload.JumlahPesanan;
                    GetData.HargaSatuan = Payload.HargaSatuan;
                    GetData.Nik = Payload.Nik;
                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<Pesanan> Payload)
        {
            try
            {
                var ListDataUpdate = new List<Pesanan>();

                var GetData = DBContext.Pesanans.Where(p => Payload.Select(x => x.KodeTransaksi).Contains(p.KodeTransaksi)).Select(Hasil => new Pesanan
                {
                    Id = Hasil.Id,
                    KodeTransaksi = Hasil.KodeTransaksi,
                    KodePemesan = Hasil.KodePemesan,
                    KodeMenu = Hasil.KodeMenu,
                    JumlahPesanan = Hasil.JumlahPesanan,
                    HargaSatuan = Hasil.HargaSatuan,
                    Nik = Hasil.Nik,
                }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().Id;
                        var KodeTransaksi = Payload?.Where(x => x?.KodeTransaksi == Obj?.KodeTransaksi).FirstOrDefault()?.KodeTransaksi;
                        var KodePemesan = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().KodePemesan;
                        var KodeMenu = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().KodeMenu;
                        var JumlahPesanan = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().JumlahPesanan;
                        var HargaSatuan = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().HargaSatuan;
                        var Nik = Payload.Where(x => x.KodeTransaksi == Obj.KodeTransaksi).FirstOrDefault().Nik;

                        var ObjectUpdate = new Pesanan
                        {
                            Id = Id,
                            KodeTransaksi = KodeTransaksi,
                            KodePemesan = KodePemesan,
                            KodeMenu = KodeMenu,
                            JumlahPesanan = JumlahPesanan,
                            HargaSatuan = HargaSatuan,
                            Nik = Nik
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataByKodeTransaksi : Success
        [HttpDelete("{KodeTransaksi}"), Produces("application/json")]
        public IActionResult DeleteDataByKodeTransaksi(string KodeTransaksi)
        {
            try
            {
                var GetData = DBContext.Pesanans.Where(x => x.KodeTransaksi == KodeTransaksi).FirstOrDefault();
                //var GetData = DBContext.DaftarPemesans.Include(x => x.NikNavigation).Where(x => x.KodePemesan == KodePemesan).FirstOrDefault();
                if (GetData != null)
                {
                    DBContext.Pesanans.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByKodeTransaksi : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodeTransaksi(List<string> KodeTransaksi)
        {
            try
            {
                var ObjectDelete = new Pesanan();
                var ListDataDelete = new List<Pesanan>();
                var StringJoin = string.Join(",", KodeTransaksi);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetData = (from x in DBContext.Pesanans.Where(x => x.KodeTransaksi.Contains(obj))
                                   select new Pesanan
                                   {
                                       Id = x.Id,
                                       KodeTransaksi = x.KodeTransaksi,
                                       KodePemesan = x.KodePemesan,
                                       KodeMenu = x.KodeMenu,
                                       JumlahPesanan = x.JumlahPesanan,
                                       HargaSatuan = x.HargaSatuan,
                                       Nik = x.Nik
                                   }).AsQueryable();

                    if (GetData.Count() > 0)
                    {
                        foreach (var x in GetData)
                        {
                            ObjectDelete.Id = x.Id;
                            ObjectDelete.KodeTransaksi = x.KodeTransaksi;
                            ObjectDelete.KodePemesan = x.KodePemesan;
                            ObjectDelete.KodeMenu = x.KodeMenu;
                            ObjectDelete.JumlahPesanan = x.JumlahPesanan;
                            ObjectDelete.HargaSatuan = x.HargaSatuan;
                            ObjectDelete.Nik = x.Nik;

                            ListDataDelete.Add(ObjectDelete);
                            DBContext.Pesanans.Entry(ObjectDelete).State = EntityState.Deleted;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = GetData
                        };
                        return NotFound(Result);
                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = ListDataDelete
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByKodeTransaksi : Success
        [HttpGet("{KodeTransaksi}"), Produces("application/json")]
        public IActionResult GetDataByKodeTransaksi(string KodeTransaksi)
        {
            try
            {
                var GetData = DBContext.Pesanans.FirstOrDefault(x => x.KodeTransaksi == KodeTransaksi);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<Pesanan>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.Pesanans.Where(x => x.KodeTransaksi == Obj).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleInt : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            try
            {
                var ListDataMultiple = new List<Pesanan>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.Pesanans.Where(x => x.Id == Convert.ToInt64(Obj)).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetAllDataJoin_DaftarPemesan_Pesanan For My_Portfolio : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataJoin_DaftarPemesan_Pesanan()
        {
            try
            {
                var GetData = (from A in DBContext.DaftarPemesans.AsSingleQuery()
                               join B in DBContext.Pesanans.AsSingleQuery() on A.KodePemesan equals B.KodePemesan
                               join C in DBContext.MasterMenus on A.KodeMenu equals C.KodeMenu
                               select new
                               {
                                   Id = B.Id == 0 ? 0 : B.Id,
                                   KodeTransaksi = B.KodeTransaksi == null ? string.Empty : B.KodeTransaksi,
                                   KodePemesan = B.KodePemesan == null ? string.Empty : B.KodePemesan,
                                   NamaPemesan = A.NamaPemesan == null ? string.Empty : A.NamaPemesan,
                                   JenisPesanan = C.JenisMenu == null ? string.Empty : C.JenisMenu,
                                   KodeMenu = B.KodeMenu == null ? string.Empty : B.KodeMenu,
                                   NamaMenu = C.NamaMenu == null ? string.Empty : C.NamaMenu,
                                   JumlahPesanan = B.JumlahPesanan == 0 ? 0 : B.JumlahPesanan,
                                   HargaSatuan = B.HargaSatuan == 0 ? string.Empty : "Rp. " + string.Format("{0:#,##0}", B.HargaSatuan)
                               }).Distinct().OrderBy(x => x.Id).AsQueryable();

                if (GetData.Count() > 0)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return Ok(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetDataById For My_Portfolio : Success
        [HttpPost("{Id}"), Produces("application/json")]
        public IActionResult GetDataById(long Id)
        {
            try
            {
                var GetData = DBContext.Pesanans.FirstOrDefault(x => x.Id == Id);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion
    }
}
