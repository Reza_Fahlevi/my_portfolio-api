﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using My_API.Connection;
using My_API.Models;

namespace My_API.Controllers
{
    [Route("API/[controller]/[action]")]
	[ApiController]
	public class MasterMenuController : ControllerBase
	{
        #region Property Connection Database
        static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
        //public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
        public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
        public static string GetConnectionStrings = GetConnection == "" ? "" : GetConnectionDevelopment;

        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<MasterMenuController> Logger;

        public MasterMenuController(ILogger<MasterMenuController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        #region Property Result
        public static dynamic Result;
        public static List<dynamic>[] Null;
        #endregion

        /// <summary>
        /// MasterMenu API CRUD : Create, Read, Update, Delete

        #region GetAllDataMasterMenu : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterMenu()
        {
            try
            {
                //var GetData = DBContext.MasterMenus.OrderBy(x => x.Id).ToList();
                var GetData = DBContext.MasterMenus.OrderBy(x => x.Id).AsQueryable();
                if (GetData.Count() > 0)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddData(MasterMenu Payload)
        {
            try
            {
                var ObjectInsert = new MasterMenu()
                {
                    KodeMenu = Payload.KodeMenu,
                    NamaMenu = Payload.NamaMenu,
                    JenisMenu = Payload.JenisMenu,
                    HargaSatuan = Payload.HargaSatuan
                };

                #region Call the database connection and then insert data.
                DBContext.Add(ObjectInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert data !",
                    Data = ObjectInsert
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region AddDataMultiple : Success
        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<MasterMenu> Payload)
        {
            try
            {
                var ListDataInsert = new List<MasterMenu>();
                var ListDataMultiple = new List<MasterMenu>();

                foreach (var x in Payload)
                {
                    ListDataInsert.Add(new MasterMenu
                    {
                        KodeMenu = x.KodeMenu,
                        NamaMenu = x.NamaMenu,
                        JenisMenu = x.JenisMenu,
                        HargaSatuan = x.HargaSatuan
                    });
                }
                ListDataMultiple.AddRange(ListDataInsert);

                #region Call the database connection and then insert multiple data.
                DBContext.MasterMenus.AddRange(ListDataInsert);
                DBContext.SaveChanges();
                #endregion

                Result = new
                {
                    Message = "Successfully save/insert multiple data !",
                    Data = ListDataMultiple
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region UpdateData : Success
        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(MasterMenu Payload)
        {
            #region Example 1 : With List
            //try
            //{
            //    var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Payload.KodeMenu).ToList();
            //    if (GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        foreach (var x in GetData)
            //        {
            //            x.KodeMenu = Payload.KodeMenu;
            //            x.NamaMenu = Payload.NamaMenu;
            //            x.JenisMenu = Payload.JenisMenu;
            //            x.HargaSatuan = Payload.HargaSatuan;
            //            DBContext.SaveChanges();
            //        }
            //        Result = new
            //        {
            //            Message = "Successfully update/modified data !",
            //            Data = GetData
            //        };
            //        return StatusCode(200, Result);
            //    }
            //    else
            //    {
            //        Result = new
            //        {
            //            Message = "Data not found !",
            //            Data = GetData
            //        };
            //        return NotFound(Result);
            //    }
            //}
            //catch (Exception Error)
            //{
            //    Result = new
            //    {
            //        StatusCode = 500,
            //        Message = Error.Message,
            //        Data = Null
            //    };
            //    Console.WriteLine(Error.Message);
            //    return StatusCode(500, Result);
            //}
            //finally
            //{
            //}
            #endregion

            #region Example 2 : With Single Data    
            try
            {
                var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Payload.KodeMenu).FirstOrDefault();
                if (GetData != null || GetData.KodeMenu != null || GetData.KodeMenu == Payload.KodeMenu || GetData.KodeMenu.Contains(Payload.KodeMenu))
                {
                    GetData.KodeMenu = Payload.KodeMenu;
                    GetData.NamaMenu = Payload.NamaMenu;
                    GetData.JenisMenu = Payload.JenisMenu;
                    GetData.HargaSatuan = Payload.HargaSatuan;

                    DBContext.SaveChanges();
                    Result = new
                    {
                        Message = "Successfully update/modified data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
            #endregion
        }
        #endregion

        #region UpdateDataMultiple : Success
        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<MasterMenu> Payload)
        {
            try
            {
                var ListDataUpdate = new List<MasterMenu>();

                var GetData = DBContext.MasterMenus.Where(p => Payload.Select(x => x.KodeMenu).Contains(p.KodeMenu)).Select(Hasil => new MasterMenu
                {
                    Id = Hasil.Id,
                    KodeMenu = Hasil.KodeMenu,
                    NamaMenu = Hasil.NamaMenu,
                    JenisMenu = Hasil.JenisMenu,
                    HargaSatuan = Hasil.HargaSatuan,
                }).AsQueryable();

                if (GetData.Count() > 0)
                {
                    foreach (var Obj in GetData)
                    {
                        var Id = GetData.Where(x => x.KodeMenu == Obj.KodeMenu).FirstOrDefault().Id;
                        var KodeMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.KodeMenu;
                        var NamaMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.NamaMenu;
                        var JenisMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.JenisMenu;
                        var HargaSatuan = Payload?.Where(x => x.KodeMenu == Obj.KodeMenu).FirstOrDefault()?.HargaSatuan;

                        var ObjectUpdate = new MasterMenu
                        {
                            Id = Id,
                            KodeMenu = KodeMenu,
                            NamaMenu = NamaMenu,
                            JenisMenu = JenisMenu,
                            HargaSatuan = HargaSatuan,
                        };
                        ListDataUpdate.Add(ObjectUpdate);

                        #region Call the database connection and then modify the data.
                        DBContext.Update(ObjectUpdate).Property(x => x.Id).IsModified = false;
                        DBContext.SaveChanges();
                        #endregion
                    }
                    Result = new
                    {
                        Message = "Successfully update/modified multiple data !",
                        Data = ListDataUpdate
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeteleDataByKodeMenu : Success
        [HttpDelete("{KodeMenu}"), Produces("application/json")]
        public IActionResult DeleteDataByKodeMenu(string KodeMenu)
        {
            try
            {
                var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == KodeMenu).FirstOrDefault();

                if (GetData != null)
                {
                    DBContext.MasterMenus.Entry(GetData).State = EntityState.Deleted;
                    DBContext.SaveChanges();

                    Result = new
                    {
                        Message = "Successfully deleted data !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region DeleteDataMultipleByKodeMenu : Success
        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodeMenu(List<string> KodeMenu)
        {
            try
            {
                var ObjectDelete = new MasterMenu();
                var ListDataDelete = new List<MasterMenu>();
                var StringJoin = string.Join(",", KodeMenu);
                var SplitData = StringJoin.Split(",");

                foreach (var obj in SplitData)
                {
                    var GetData = (from x in DBContext.MasterMenus.Where(x => x.KodeMenu.Contains(obj))
                                   select new MasterMenu
                                   {
                                       Id = x.Id,
                                       KodeMenu = x.KodeMenu,
                                       NamaMenu = x.NamaMenu,
                                       JenisMenu = x.JenisMenu,
                                       HargaSatuan = x.HargaSatuan
                                   }).AsQueryable();

                    if (GetData.Count() > 0)
                    {
                        foreach (var x in GetData)
                        {
                            ObjectDelete.Id = x.Id;
                            ObjectDelete.KodeMenu = x.KodeMenu;
                            ObjectDelete.NamaMenu = x.NamaMenu;
                            ObjectDelete.JenisMenu = x.JenisMenu;
                            ObjectDelete.HargaSatuan = x.HargaSatuan;

                            ListDataDelete.Add(ObjectDelete);
                            DBContext.MasterMenus.Entry(ObjectDelete).State = EntityState.Deleted;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        Result = new
                        {
                            Message = "Data not found !",
                            Data = GetData
                        };
                        return NotFound(Result);
                    }
                }
                Result = new
                {
                    Message = "Successfully deleted multiple data !",
                    Data = ListDataDelete
                };
                return StatusCode(200, Result);
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataByKodeMenu : Success
        [HttpGet("{KodeMenu}"), Produces("application/json")]
        public IActionResult GetDataByKodeMenu(string KodeMenu)
        {
            try
            {
                var GetData = DBContext.MasterMenus.FirstOrDefault(x => x.KodeMenu == KodeMenu);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleString : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            try
            {
                var ListDataMultiple = new List<MasterMenu>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Obj).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region GetDataMultipleInt : Success
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            try
            {
                var ListDataMultiple = new List<MasterMenu>();
                var StringJoin = string.Join(",", Payload);
                var SplitData = StringJoin.Split(",");

                if (Payload.Count() > 0)
                {
                    foreach (var Obj in SplitData)
                    {
                        var GetData = DBContext.MasterMenus.Where(x => x.Id == Convert.ToInt64(Obj)).SingleOrDefault();
                        if (GetData == null)
                        {
                            Result = new
                            {
                                Message = "Data not found !",
                                Data = GetData
                            };
                            return NotFound(Result);
                        }
                        else
                        {
                            ListDataMultiple.Add(GetData);
                        }
                    }
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = ListDataMultiple
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = ListDataMultiple
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion

        #region Consume API GetDataById For My_Portfolio : Success
        [HttpPost("{Id}"), Produces("application/json")]
        public IActionResult GetDataById(long Id)
        {
            try
            {
                var GetData = DBContext.MasterMenus.FirstOrDefault(x => x.Id == Id);
                if (GetData != null)
                {
                    Result = new
                    {
                        Message = "Data successfully found !",
                        Data = GetData
                    };
                    return StatusCode(200, Result);
                }
                else
                {
                    Result = new
                    {
                        Message = "Data not found !",
                        Data = GetData
                    };
                    return NotFound(Result);
                }
            }
            catch (Exception Error)
            {
                Result = new
                {
                    StatusCode = 500,
                    Message = Error.Message,
                    Data = Null
                };
                Console.WriteLine(Error.Message);
                return StatusCode(500, Result);
            }
            finally
            {
            }
        }
        #endregion
    }
}
